package com.whishlistbike.whishlistbike;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.whishlistbike.whishlistbike.View.HomeActivity;

public class MainActivity extends AppCompatActivity {

    public final Context context = this;

    EditText editTextEmail, editTextPassword;
    Button buttonEnter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonEnter = findViewById(R.id.buttonEnter);
        buttonEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try
                {
                    editTextEmail = findViewById(R.id.editTextEmail);
                    if(editTextEmail.getText().length() == 0)
                        Toast.makeText(context, R.string.message_email_login, Toast.LENGTH_LONG).show();

                    editTextPassword = findViewById(R.id.editTextPassword);
                    if(editTextPassword.getText().length() == 0)
                        Toast.makeText(context, R.string.message_password_login, Toast.LENGTH_LONG).show();

                    if(editTextEmail.getText().length() != 0 && editTextPassword.getText().length() != 0)
                    {
                        Intent intent = new Intent(context, HomeActivity.class);
                        MainActivity.this.startActivity(intent);

                    }

                }catch (Exception e)
                {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
