package com.whishlistbike.whishlistbike.View;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.whishlistbike.whishlistbike.MainActivity;
import com.whishlistbike.whishlistbike.R;

public class HomeActivity extends AppCompatActivity {

    public final Context context = this;

    TextView textViewSearchProduct;
    ScrollView scrollViewSearchProduct;
    FloatingActionButton floatingActionButtonHome;
    Button buttonDetail;
    Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        buttonDetail = findViewById(R.id.buttonDetail);
        buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, DetailActivity.class);
                HomeActivity.this.startActivity(intent);
            }
        });

        floatingActionButtonHome = findViewById(R.id.floatingActionButtonHome);
        floatingActionButtonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, MyFavoritsActivity.class);
                HomeActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Condição para retornar a activity anterior
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        //Condição para sair da aplição e retornar a activity principal
        if (id == R.id.action_intent_logout){
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
