package com.whishlistbike.whishlistbike.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.whishlistbike.whishlistbike.MainActivity;
import com.whishlistbike.whishlistbike.R;

public class MyFavoritsActivity extends AppCompatActivity {

    public final Context context = this;

    Button buttonContactList;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorits);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonContactList = findViewById(R.id.buttonContactList);
        buttonContactList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, ContactListActivity.class);
                MyFavoritsActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Condição para retornar a activity anterior
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        //Condição para sair da aplição e retornar a activity principal
        if (id == R.id.action_intent_logout){
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
