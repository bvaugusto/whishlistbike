package com.whishlistbike.whishlistbike.View;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.whishlistbike.whishlistbike.MainActivity;
import com.whishlistbike.whishlistbike.R;

public class ContactListActivity extends AppCompatActivity {

    public final Context context = this;

    EditText editTextNameClient, editTexMailClient, editTextContactClient;
    Button buttonSendWhishList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buttonSendWhishList = findViewById(R.id.buttonSendWhishList);
        buttonSendWhishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    editTextNameClient = findViewById(R.id.editTextNameClient);
                    if(editTextNameClient.getText().length() == 0)
                        Toast.makeText(context, "Campo nome é obrigatório!", Toast.LENGTH_LONG).show();

                    editTexMailClient = findViewById(R.id.editTexMailClient);
                    if(editTexMailClient.getText().length() == 0)
                        Toast.makeText(context, "Campo e-mail é obrigatório!", Toast.LENGTH_LONG).show();

                    editTextContactClient = findViewById(R.id.editTextContactClient);
                    if(editTextContactClient.getText().length() == 0)
                        Toast.makeText(context, "Campo contato é obrigatório!", Toast.LENGTH_LONG).show();

                    Toast.makeText(context, "Solicitação enviada com sucesso!", Toast.LENGTH_LONG).show();

                }catch (Exception e )
                {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Condição para retornar a activity anterior
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        //Condição para sair da aplição e retornar a activity principal
        if (id == R.id.action_intent_logout){
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
